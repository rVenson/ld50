extends Node

export var start_scene : PackedScene

func start_game():
	if (!start_scene) : return
	get_tree().change_scene_to(start_scene)

func exit_game():
	get_tree().quit(0)

func _on_ButtonStart_pressed():
	start_game()

func _on_ButtonExit_pressed():
	exit_game()
