extends Node

onready var _scene = $Scene
onready var _ui = $CanvasLayer/UI
onready var _ui_anim = $CanvasLayer/AnimationPlayer
onready var _final_screen = $CanvasLayer/FinalScreen
onready var _dialog = $CanvasLayer/Dialog
onready var _music = $AudioStreamPlayer
onready var start_time = OS.get_ticks_msec()
onready var total_time = 0
onready var _player = $Scene/Icecube

func _ready():
	set_physics_process(false)
	_dialog.hide()
	yield(get_tree().create_timer(1.5), "timeout")
	yield(_dialog.show_text('I know it may sound a little cliche, but my biggest dream was to meet the sun'), 'completed')
	yield(_dialog.show_text('My planet keeps heating up, and probably I didn\'t have much time to live'), 'completed')
	yield(_dialog.show_text('So I decided to go on a journey and fulfill my dream.'), 'completed')
	set_physics_process(true)
	_player.pause(false)

func process_time():
	total_time = OS.get_ticks_msec() - start_time

func process_temperature(delta):
	if (is_instance_valid(_player)):
		_player.current_temperature += delta

func _process(delta):
	_ui.update_timer(total_time)
	_ui.update_temperature(_player.current_temperature)

func _physics_process(delta):
	process_time()
	process_temperature(delta)

func _on_FinishRespawn_activated():
	set_physics_process(false)
	_music.stop()
	_music.stream = load("res://Sounds/Music/the_end.ogg")
	_music.play()
	yield(_scene.end_game_sequence(), 'completed')
	_ui_anim.play("final_screen")
	yield(_ui_anim, "animation_finished")
	_final_screen.start_final_sequence()
	_final_screen.set_score(total_time)
	_music.stop()
	_music.stream = load("res://Sounds/Music/final_theme.ogg")
	_music.play()

func _on_Scene_dialog(dialog_text):
	set_physics_process(false)
	_player.pause()
	yield(_dialog.show_text(dialog_text), 'completed')
	_player.pause(false)
	set_physics_process(true)
