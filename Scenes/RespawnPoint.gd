extends Node2D

signal activated
export var activable = true
export var dialog_text : String
onready var _anim = $AnimatedSprite
onready var _position = $Position2D

func _on_Area2D_body_entered(body):
	if (!activable) : return
	if (body is Icecube):
		_anim.play('open')
		emit_signal("activated", _position, dialog_text)
