extends Node2D

signal pick

func _on_Area2D_body_entered(body):
	if (body is Icecube):
		emit_signal("pick", self)
