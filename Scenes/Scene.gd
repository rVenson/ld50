extends Node2D

signal dialog
onready var _sun = $Objects/Sun
onready var _finish_anim = $AnimationPlayer
onready var _start_respawn = $Respawns/StartingRespawn
onready var _camera = $Camera2D
onready var _player = $Icecube
onready var current_respawn = _start_respawn

func _unhandled_key_input(event):
	if (event.is_action_pressed('reset')):
		respawn_player()

func _ready():
	_camera.set_player(_player)
	respawn_player()
	yield(get_tree(), "idle_frame")
	_player.pause()

func respawn_player():
	_player.position_to_respawn = current_respawn.global_position

func end_game_sequence():
	_player.pause()
	_finish_anim.play('sun_intro')
	yield(_finish_anim, "animation_finished")
	yield(get_tree().create_timer(1), "timeout")
	_sun.anger()
	yield(get_tree().create_timer(3), "timeout")
	_player.death_animation()
	yield(get_tree().create_timer(2), "timeout")
	_sun.happy()
	_finish_anim.play("sun_final")

func _on_Icecube_damage():
	respawn_player()

func _on_LockedDoor_unlock(door):
	var player = _player
	var player_parent = player.get_parent()
	player_parent.remove_child(player)
	_camera.set_physics_process(false)
	var current_camera_tile = _camera.get_current_camera_tile()
	var door_tile_position = _camera.get_camera_tile_from_position(door.global_position)
	door_tile_position.x -= 1
	_camera.update_camera_tile(door_tile_position)
	yield(get_tree().create_timer(4), "timeout")
	_camera.update_camera_tile(current_camera_tile)
	player_parent.add_child(player)
	_camera.set_physics_process(true)

func _on_RespawnPoint_activated(position_node, dialog_text = ""):
	if (current_respawn != position_node):
		current_respawn = position_node
		_player.reset_temperature()
		AudioManager.play_effect('checkpoint', 1)
		if (!dialog_text.empty()) : emit_signal('dialog', dialog_text)
