extends Node2D

func _on_Area2D_body_entered(body):
	if (body is Icecube):
		body.make_jump = true
		body.jump_direction = Vector2.UP
		body.jump_power = 2
		AudioManager.play_effect('elastic')
 
