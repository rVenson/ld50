extends Node2D

onready var _anim = $AnimatedSprite

func happy():
	_anim.play('happy')

func anger():
	_anim.play('anger')
