extends Camera2D

onready var window_size = Vector2(400, 300)
onready var player : Node2D = null

func _physics_process(delta):
	if (!player || !is_instance_valid(player)) : return
	var player_tile = get_camera_tile_from_position(player.global_position)
	update_camera_tile(player_tile)

func update_camera_tile(tile_position):
	position = tile_position * window_size

func get_current_camera_tile():
	var current_tile = Vector2()
	current_tile.x = int(position.x / window_size.x)
	current_tile.y = int(position.y / window_size.y)
	return current_tile

func get_camera_tile_from_position(position):
	var camera_tile = Vector2()
	camera_tile.x = int(position.x / window_size.x)
	camera_tile.y = int(position.y / window_size.y)
	return camera_tile

func set_player(player):
	self.player = player
