extends Node2D

signal activated

func _on_Area2D_body_entered(body):
	if (body is Icecube):
		emit_signal("activated")
