extends Node2D

export var target_x = 15
onready var _static = $StaticBody2D
onready var _collider = $StaticBody2D/CollisionShape2D
onready var _area_collider = $Area2D/CollisionShape2D
onready var _anim = $StaticBody2D/AnimatedSprite

func wreck():
	_anim.play('wreck')
	yield(_anim, "animation_finished")
	_collider.disabled = true
	AudioManager.play_effect('crash')

func unwreck():
	_anim.play('default')
	_collider.disabled = false

func _on_Area2D_body_entered(body):
	if (body is Icecube):
		_area_collider.disabled = true
		yield(get_tree().create_timer(1), "timeout")
		wreck()
		yield(get_tree().create_timer(4), "timeout")
		unwreck()
		_area_collider.disabled = false
