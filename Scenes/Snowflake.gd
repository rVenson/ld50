extends Area2D

export var rotation_speed = 100

func _ready():
	randomize()
	rotation_degrees = rand_range(0, 360)

func _physics_process(delta):
	rotate(deg2rad(delta * rotation_speed))

func set_enabled(value):
	set_deferred("monitorable", value) 
	set_deferred("monitoring", value) 
	self.visible = value

func _on_Snowflake_body_entered(body):
	if (body is Icecube):
		set_enabled(false)
		AudioManager.play_effect('powerup')
		body.add_life()
		yield(get_tree().create_timer(7), 'timeout')
		set_enabled(true)
