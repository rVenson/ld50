extends Control

onready var _tween = $Tween
onready var _dialog = $Box/VBoxContainer/Label
onready var _continue = $Box/VBoxContainer/Continue
onready var _button_verification = $ButtonVerification

func show_text(text):
	show()
	_continue.modulate.a = 0
	_dialog.visible_characters = 0
	_dialog.text = text
	var duration = max(1, text.length() / 100)
	_tween.interpolate_property(_dialog, "visible_characters", 0, text.length(), duration)
	_tween.start()
	yield(get_tree().create_timer(duration), "timeout")
	_continue.modulate.a = 1
	yield(_button_verification, "button_pressed")
	AudioManager.play_effect('menu_accept', 1)
	hide()
