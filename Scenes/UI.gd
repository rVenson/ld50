extends Control

onready var _tween = $Tween
onready var _foreground = $ColorRect
onready var _timer = $HBoxContainer/VBoxContainer/Timer
onready var _temperature = $HBoxContainer/VBoxContainer/Temperature

func _ready():
	_tween.interpolate_property(_foreground, 'modulate:a', 1, 0, 2)
	_tween.start()

func update_timer(value):
	var time = Utils.time_convert(value)
	_timer.text = "%02d:%02d:%02d.%03d" % [
		time.hours,
		time.minutes,
		time.seconds,
		time.milliseconds
	]

func update_temperature(value):
	_temperature.text = "%d Celcius" %value
	if (value > -10):
		_temperature.add_color_override("font_color", Color.red)
		return
	if (value > -20):
		_temperature.add_color_override("font_color", Color.yellow)
		return
	_temperature.add_color_override("font_color", Color.white)
