extends Node

onready var _effects = $Effects
onready var effect_list = {
	"checkpoint": preload("res://Sounds/Effects/checkpoint.wav"),
	"crash": preload("res://Sounds/Effects/crash.wav"),
	"death": preload("res://Sounds/Effects/death.wav"),
	"elastic": preload("res://Sounds/Effects/elastic.wav"),
	"jump": preload("res://Sounds/Effects/jump.wav"),
	"menu_accept": preload("res://Sounds/Effects/menu_accept.wav"),
	"powerup": preload("res://Sounds/Effects/powerup.wav"),
	"tackle": preload("res://Sounds/Effects/tackle.wav"),
}

func play_effect(effect_name, pitch = null):
	if (!effect_list.has(effect_name)) : return
	var audio_player = AudioStreamPlayer.new()
	audio_player.stream = effect_list[effect_name]
	audio_player.bus = "Effect"
	_effects.add_child(audio_player)
	audio_player.play()
	if (pitch):
		audio_player.pitch_scale = pitch
	else:
		audio_player.pitch_scale = rand_range(0.8, 1.2)
	yield(audio_player, "finished")
	audio_player.queue_free()
