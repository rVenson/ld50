extends Node2D

signal unlock

func unlock():
	emit_signal('unlock', self)
	yield(get_tree().create_timer(2), "timeout")
	queue_free()

func _on_Key_pick(key):
	unlock()
	key.queue_free()
