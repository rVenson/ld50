extends Node2D

export onready var speed = 1
export var target = Vector2(100, 0)
export var next_target = Vector2(0, 0)
onready var _static = $StaticBody

func _physics_process(delta):
	var direction = _static.transform.origin.direction_to(target)
	var distance = _static.transform.origin.distance_to(target)
	_static.move_and_collide(direction * speed)
	if (distance < 1):
		swap_target()

func swap_target():
	var aux_target = target
	target = next_target
	next_target = aux_target
