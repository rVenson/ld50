extends Node

signal button_pressed

func _unhandled_key_input(event):
	emit_signal('button_pressed')
