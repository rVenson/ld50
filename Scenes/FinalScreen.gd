extends Control

onready var _credits = $Credits
onready var _score = $Credits/FinalScore
onready var _tween = $Tween

func set_score(value):
	var time = Utils.time_convert(value)
	_score.text = "You finish the game in %02d:%02d:%02d.%03d" % [
		time.hours,
		time.minutes,
		time.seconds,
		time.milliseconds
	]

func start_final_sequence():
	for node in _credits.get_children():
		node.show()
		_tween.interpolate_property(node, "modulate:a", 0, 1, 5)
		_tween.start()
		yield(_tween, "tween_completed")

func _on_Button_pressed():
	get_tree().change_scene("res://Scenes/MainMenu/MainMenu.tscn")
