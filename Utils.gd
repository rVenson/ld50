class_name Utils

static func time_convert(msec):
	var sec = msec / 1000
	var time = {
		"milliseconds": msec % 1000,
		"seconds": sec % 60,
		"minutes": (sec / 60) % 60,
		"hours": ((sec/60) / 60) % 24,
		"days": ((sec/60) / 60) / 24
	}
	return time
