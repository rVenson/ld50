extends KinematicBody2D

onready var _direction_line = $DirectionLine
onready var player_direction = Vector2(0, 0)
onready var jump_direction = Vector2(0, 0)
onready var jump_pressed = false
onready var jumping = false
onready var on_floor = false
onready var jump_value = 1
onready var direction_speed = 0.05
onready var move_speed = 100
onready var jump_power = 300
onready var gravity_power = 10
onready var steering_power = 1

func process_gravity(delta):
	player_direction.y += gravity_power

func process_floor_check(delta):
	var landed = is_on_floor() != on_floor
	on_floor = is_on_floor()
	if (!landed) : return
	jumping = false
	jump_value = 0

func process_jump_direction(delta):
	jump_direction.x = Input.get_action_strength("directional_right") - Input.get_action_strength('directional_left')
	_direction_line.rotation += jump_direction.x * direction_speed

func process_jump_preparation(delta):
	if (jumping) : return
	jump_pressed = Input.is_action_pressed('jump')
	if (jump_pressed) : jump_value = min(2, delta + jump_value)
	if (Input.is_action_just_released('jump')):
		jumping = true

func process_jump(delta):
	if (jumping):
		var jump_force = jump_power * jump_value * -1
		var origin : Vector2 = _direction_line.points[0]
		var destination : Vector2 = _direction_line.points[1]
		var final_direction = origin.direction_to(destination)
		player_direction = final_direction.normalized() * jump_force
		jump_value = max(0, jump_value - delta)

func process_move(delta):
	player_direction.x = Input.get_action_strength('right') - Input.get_action_strength('left')
	player_direction.x *= move_speed
	move_and_slide(player_direction, Vector2.UP)
	player_direction.x = 0

func _physics_process(delta):
	process_gravity(delta)
	process_floor_check(delta)
	process_jump_direction(delta)
	process_jump_preparation(delta)
	process_jump(delta)
	process_move(delta)
