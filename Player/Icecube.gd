extends RigidBody2D
class_name Icecube

signal damage
onready var _sprite = $Sprite
onready var _direction_line = $DirectionLine
export var _particles_resource : PackedScene
export var life = 10
export var JUMP_BASE_FORCE = 7000
export var JUMP_MAX_POWER = 2
export var INITIAL_TEMPERATURE = -30
export var DEATH_TEMPERATURE = 0
export var INITIAL_PARTICLE_AMOUNT = 5
export var FINAL_PARTICLE_AMOUNT = 30
onready var current_temperature = INITIAL_TEMPERATURE
onready var direction_speed = 2
onready var jump_direction = Vector2(0, 0)
onready var jump_power = 1.0
onready var jump_power_direction = 1
onready var make_jump = false
onready var position_to_respawn = null

func process_respawn(state):
	reset_temperature()
	state.linear_velocity = Vector2(0, 0)
	state.transform.origin = position_to_respawn
	position_to_respawn = null
	_sprite.play('default')

func _integrate_forces(state):
	if (position_to_respawn):
		process_respawn(state)

func _unhandled_input(event):
	if (event.is_action_released('jump')):
		make_jump = true

func pause(value = true):
	set_physics_process(!value)
	set_process_unhandled_input(!value)
	sleeping = value
	_direction_line.visible = !value

func reset_temperature():
	current_temperature = INITIAL_TEMPERATURE

func add_life(value = 5):
	current_temperature -= value

func get_direction():
	var origin = _direction_line.points[1]
	var destination = _direction_line.points[0]
	var direction : Vector2 = destination - origin
	return direction

func process_death():
	if (current_temperature > DEATH_TEMPERATURE):
		death()

func process_direction():
	var action_direction = Input.get_action_strength("directional_right") - Input.get_action_strength('directional_left')
	var direction = get_direction()
	var new_pos = direction.rotated(deg2rad(action_direction * direction_speed))
	_direction_line.points[0] = new_pos

func update_jump_direction():
	if (make_jump) : return
	var origin = _direction_line.points[1]
	var destination = _direction_line.points[0]
	var direction : Vector2 = destination - origin
	jump_direction = get_direction()

func process_jump_power(delta):
	if (Input.is_action_pressed("jump")):
		jump_power = max(0, min(JUMP_MAX_POWER, jump_power + (delta * 2 * jump_power_direction)))
#		if (jump_power == JUMP_MAX_POWER):
#			jump_power_direction = -1
#		if (jump_power == 0):
#			jump_power_direction = 1

func update_jump_power():
	var line_width = max(1, jump_power * 10)
	_direction_line.width_curve.set_point_value(0, line_width)

func process_jump():
	if (make_jump):
		applied_force = Vector2(0, 0)
		set_axis_velocity(Vector2.UP)
		make_jump = false
		apply_central_impulse(jump_direction.normalized() * jump_power * JUMP_BASE_FORCE)
		jump_power = 0
		spawn_animation()
		AudioManager.play_effect('jump', jump_power)

func spawn_animation():
	var _particles = _particles_resource.instance()
	_particles.amount = 30 - (0.83 * abs(current_temperature))
	add_child(_particles)
	_particles.emitting = true
	yield(get_tree().create_timer(2), "timeout")
	if (is_instance_valid(_particles)):
		_particles.queue_free()

func death():
	pause()
	sleeping = true
	_sprite.play('death')
	AudioManager.play_effect('death')
	yield(_sprite, "animation_finished")
	emit_signal('damage')
	sleeping = false
	pause(false)
	current_temperature = INITIAL_TEMPERATURE

func death_animation():
	_sprite.play('death')

func _physics_process(delta):
	process_death()
	process_direction()
	update_jump_direction()
	process_jump_power(delta)
	update_jump_power()
	process_jump()

func _on_Icecube_body_entered(body : Node):
	AudioManager.play_effect("tackle")
	if (body.is_in_group('damageable')):
		death()
